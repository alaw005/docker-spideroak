FROM        ubuntu:16.04
MAINTAINER  Adam Lawrence <alaw005@gmail.com>

# Required for nano to work
ENV TERM xterm

# Add spideroak repository
RUN echo 'deb http://APT.spideroak.com/ubuntu-spideroak-hardy/ release restricted' > \
                /etc/apt/sources.list.d/spideroak.com.sources.list \
    && apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 573E3D1C51AE1B3D
    
# Install spideroak and dependencies. 

# NB: supervisor is used to keep container running when spideroak processes 
# finish (to enable configuration to be run first).
# [TODO: Could replace config with volumes to fix?]

RUN apt-get update \
    && apt-get install -y \
        nano \
        spideroakone \
        supervisor

# Copy scripts and conf files from host and make start script executable.
# NB:  start.sh currently on runs supervisord
COPY config/supervisord.conf /supervisord.conf
COPY config/start.sh /start.sh
RUN chmod +x /start.sh

# Set Hive to a volume as may hold real data when syncs with other computers
# so keep out of container. Config also in volume so can backup.
VOLUME ["/root/SpiderOak Hive/", "/root/.config/SpiderOakONE"]

# NB: Can pass commands to entrypoint script using CMD
ENTRYPOINT ["/start.sh"]
