# SpiderOakONE

## Introduction

Docker image for SpiderOakONE. Assumes that container will configured when 
first run which means all configuration is blank to start.

The earlier version of git has essentially been replaced and are now used for my
primary backup (replacing Tresorit and Crashplan - would be great if these two 
merged).

## Create docker image and configure container

NB: This docker uses supervisord to keep container running after spideroak 
    scripts finish. This is to enable configuration of spideroak before running
    spideroak process in headless mode.

### Build docker image and run container

Build docker image as follows. NB: This example maps the container /data folder
to /mnt/user on the host. 

    git clone https://alaw005@bitbucket.org/alaw005/docker-spideroak.git
    cd docker-spideroak    
    docker build --force-rm -t spideroak .
    docker run -d -v /mnt/user:/data --name spideroak spideroak

### Configure and run spideroak
  
At this stage container is doing nothing (supervisord is keeping it running. 

To use spideroak it needs to be configured manually but as install is at the 
command line we cannot use the GUI.

#### Setup spideroak

Spideroak needs to be configured with username, password and device name before it 
can be started. It is also important to add folders to be backed up.

First, configure username, password and device name. This requires user interaction.

    # Setup for user
    docker exec -it spideroak SpiderOakONE --setup=-

Then add folders to be backed up (note that uses host volume mapping from when
container run therefore root is /data not /mnt/user).
    
    # Add folders to backup, e.g:
    docker exec -it spideroak SpiderOakONE --include-dir=/data/Scratch/Test
    docker exec -it spideroak SpiderOakONE --include-dir=/data/Home
    docker exec -it spideroak SpiderOakONE --include-dir=/data/Programming
    docker exec -it spideroak SpiderOakONE --include-dir=/data/Work
    
#### Start spideroak backups

Spideroak will not run unless first setup as per above. 

    # Run in headless mode (the -d runs in background)
    docker exec -d spideroak SpiderOakONE --headless
    
### Using spideroak

You can view the config file directly with the following command (the -it is to
run interactively).

    docker exec -it spideroak nano /root/.config/SpiderOakONE/config.txt

You can run any spideroak command as showm below, where --XXX is the command.

     docker exec -it spideroak SpiderOakONE --XXX

But there is an issue in that these commands will not execute if a spideroak 
process is already running in the background or other interpretor. To stop
spideroak do the following. 

    # List all processes in container
    docker exec spideroak ps

    # Kill the process using the PID from above command for SpiderOakONE 
    docker exec spideroak kill [PID]

View log files

    # List config files where logs saved
    docker exec -it spideroak ls /root/.config/SpiderOakONE/

    # Locate latest .log file starting with spider_ and tail, e.g.
    docker exec -it spideroak tail -f /root/.config/SpiderOakONE/spider_20180109011139.log
    
# Other comments

This work started when there was no information on the internet on how to run 
spideroak as a docker container and very little even Linux/Ubuntu. My first
attempt didn't really work. There is now much better information but still 
takes some work for a non-expert like me.

Important links:

- [Spideroak command line](https://support.spideroak.com/hc/en-us/sections/115000565766-Command-Line)
- [neochrome/docker-spideroak](https://github.com/neochrome/docker-spideroak)
- [jandebleser/spideroak](https://github.com/jandebleser/spideroak)
